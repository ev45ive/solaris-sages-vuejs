import { shallowMount, mount } from '@vue/test-utils';
import PlaylistDetails from '@/components/playlists/PlaylistDetails.vue';
import { mockPlaylists } from '@/views/playlists/mockPlaylists';
import { render, screen } from '@testing-library/vue' // (or /dom, /vue, ...)
describe('PlaylistDetails.vue', () => {
  it('renders empty state', () => {
    const msg = 'Please select playlist';
    const wrapper = shallowMount(PlaylistDetails, {
      propsData: {},
    });
    expect(wrapper.text()).toMatch(msg);
  });

  it('renders example playlist', () => {
    const playlist = mockPlaylists[0]

    const wrapper = shallowMount(PlaylistDetails, {
      propsData: {
        playlist
      },
    });

    // expect(wrapper.text()).toMatch(playlist.name);
    expect(wrapper.find('[data-testid="playlist_name"]').text())
      .toMatch(playlist.name);
  });


  test('should show login form', () => {
    const playlist = mockPlaylists[0]
    render(PlaylistDetails, {
      props: {
        playlist
      }
    })
    // const input = screen.getByLabelText('Username')
    userEvent screen.getByRole('button', { name: /Edit/ })
  })



});
