## Clone
cd ..
git clone https://bitbucket.org/ev45ive/solaris-sages-vuejs.git solaris-sages-vuejs
cd solaris-sages-vuejs
npm i 
npm run serve

git pull -u origin master
git stash -u 
git pull

# Ankieta
https://tinyurl.com/2p 858 ypf
https://tinyurl.com/2p858ypf


## Zoom
https://zoom.us/

Teams:
ev45ive@gmail.com

https://us02web.zoom.us/j/ 842 655 638 33

node -v
v16.13.1

## node-gyp missing python error
sudo apt-get update     
sudo apt-get install python2.7    

ln -s /usr/bin/python2.7 /usr/bin/python2 
npm config set python  /usr/bin/python2.7


npm -v
8.1.2

code -v
1.40.1
8795a9889db74563ddd43eb0a897a2384129a619

git --version
git version 2.31.1.windows.1

## Extensions
https://marketplace.visualstudio.com/items?itemName=octref.vetur
https://marketplace.visualstudio.com/items?itemName=sdras.vue-vscode-snippets

## webstorm live templates
https://blog.jetbrains.com/webstorm/2018/01/using-and-creating-code-snippets/

## CLI
npm i -g @vue/cli 

vue --version
@vue/cli 4.5.15

vue create .

Vue CLI v4.5.15
? Please pick a preset: Manually select features
? Check the features needed for your project: Choose Vue version, Babel, TS, Router, Vuex, CSS Pre-processors, Linter, Unit, E2E
? Choose a version of Vue.js that you want to start the project with 2.x
? Use class-style component syntax? Yes
? Use Babel alongside TypeScript (required for modern mode, auto-detected polyfills, transpiling JSX)? Yes
? Use history mode for router? (Requires proper server setup for index fallback in production) No
? Pick a CSS pre-processor (PostCSS, Autoprefixer and CSS Modules are supported by default): Sass/SCSS (with node-sass)
? Pick a linter / formatter config: Airbnb
? Pick additional lint features:
? Pick a unit testing solution: Jest
? Pick an E2E testing solution: Cypress
? Where do you prefer placing config for Babel, ESLint, etc.? In dedicated config files
? Save this as a preset for future projects? (y/N) y

## Dev server
npm run serve


## Playlists module

<!-- mkdir -p src/playlists/views/PlaylistsView.vue -->
<!-- mkdir -p src/playlists/components/PlaylistList.vue -->

mkdir -p src/views/playlists/
mkdir -p src/components/playlists/

touch src/views/playlists/PlaylistsView.vue
touch src/components/playlists/PlaylistList.vue
touch src/components/playlists/PlaylistDetails.vue
touch src/components/playlists/PlaylistEditor.vue

## Music Search module

mkdir -p src/views/search/
mkdir -p src/components/search/
mkdir -p src/components/music/

touch src/views/search/SearchView.vue
touch src/components/search/SearchForm.vue
touch src/components/search/SearchResults.vue
touch src/components/music/AlbumCard.vue
touch src/components/music/ArtistCard.vue

## HTTP / REST API
https://github.com/pagekit/vue-resource
https://www.npmjs.com/package/vue-axios


## Linki
https://quicktype.io/
https://github.com/vuejs/vetur
https://chrome.google.com/webstore/detail/vuejs-devtools/nhdogjmejiglipccpnnnanhbledajbpd

https://vuejs.org/v2/cookbook/using-axios-to-consume-apis.html
https://vuejs.org/v2/guide/typescript.html#Augmenting-Types-for-Use-with-Plugins
https://docs.emmet.io/cheat-sheet/

https://bootstrap-vue.org/docs/components/list-group
https://vuetifyjs.com/en/components/lists/#shaped
https://headlessui.dev/vue/listbox

https://github.com/kaorun343/vue-property-decorator

https://bradfrost.com/blog/post/atomic-web-design/

https://www.componentdriven.org/
https://headlessui.dev/vue/menu

// components/ProductItem.vue
<CartVuexCtx :productId="props.id">
  <template v-slot="{item, addTOCart}> 
    <button @click='addTOCart' >

# VUex / Redux
https://redux.js.org/style-guide/style-guide#rule-categories


## TEsting
https://vue-test-utils.vuejs.org/
https://testing-library.com/docs/vue-testing-library/intro/

https://vuejs.org/v2/guide/testing.html#Vue-Test-Utils
https://lmiller1990.github.io/vue-testing-handbook/#what-is-this-guide
https://vuejs.org/v2/cookbook/unit-testing-vue-components.html


## Testing Library + jest.mock + msw + storybook msw

https://testing-library.com/docs/vue-testing-library/api#options
https://jestjs.io/docs/manual-mocks

https://mswjs.io/docs/getting-started/integrate/node
https://storybook.js.org/addons/msw-storybook-addon