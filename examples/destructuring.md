```ts
person: Person = {
  name: "alice",
  //     company: { name: 'ACME' },
  address: { city: "krakow" },
  scores: [1, 2, 3],
};

// getInfo = (person) => {
//     const {
//         name,
//         company:{ name: company } = {name:'Not emplyed'},
//         address:{ city },
//         scores:[,,thirdScore]
//     } = person;

//     return `${name}, ${company} from ${city} scored ${thirdScore}`
// }

getInfo = ({
  name,
  company: { name: company } = { name: "Not emplyed" },
  address: { city },
  scores: [, , thirdScore],
}: Person) => `${name}, ${company} from ${city} scored ${thirdScore}`;

getInfo(person);
```
