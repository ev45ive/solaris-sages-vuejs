```tsx

inc = (payload=1) => ({type:'INC', payload});
dec = (payload=1) => ({type:'DEC', payload});
addTodo = (payload) => ({type:'ADDTODO', payload});

reducer = (state, action) => {
    switch(action.type){
        case 'INC': return { 
        ...state, counter: state.counter + action.payload }
        case 'DEC': return { 
        ...state, counter: state.counter - action.payload }
        case 'ADDTODO': return { 
        ...state, todos: [...state.todos,  action.payload] }
        default: return state
    } 
} 
state = {
    todos:[],
    counter: 0
}
console.log( state = reducer(state, inc()) )
console.log( state = reducer(state, inc(2)) )
console.log( state = reducer(state, addTodo('kup mleko')) )
console.log( state = reducer(state, dec()) )

// [inc(),addTodo('kup mleko'),inc(2),dec()].reduce((state, action) => {
//     switch(action.type){
//         case 'INC': return {
//         ...state, counter: state.counter + action.payload }
//         case 'DEC': return {
//         ...state, counter: state.counter - action.payload }
//         case 'ADDTODO': return {
//         ...state, todos: [...state.todos,  action.payload] }
//         default: return state
//     }
// } , {
//     todos:[],
//     counter: 0
// })
VM32419:20 {todos: Array(0), counter: 1}
VM32419:21 {todos: Array(0), counter: 3}
VM32419:22 {todos: Array(1), counter: 3}
VM32419:23 {todos: Array(1), counter: 2}
undefined

```