import Vue from 'vue';

// https://vuejs.org/v2/guide/typescript.html#Augmenting-Types-for-Use-with-Plugins
declare module 'vue/types/vue' {
  // 3. Declare augmentation for Vue
  interface Vue {
    $myProperty: string
    lubieplacki: () => void
  }
}
Vue.prototype.lubieplacki = () => { }

import App from './App.vue';
import router from './router';
import { init } from './services/AuthService';
import store from './store';
Vue.config.productionTip = false;
import './components/components'

init();


import { Component } from 'vue-property-decorator'

Component.registerHooks([
  'beforeRouteEnter',
  'beforeRouteUpdate',
  'beforeRouteLeave'
]);


(window as any).app = new Vue({
  // template:'<div>alamkota</div>',
  router,
  store,
  data: {
    isOpen: true,
    message: 'test'
  },
  render: (h) => h(App),
  // render(h) {
  //   // const vnode = h(App)
  //   // vnode.children?.filter()
  //   return h('div', [
  //     h('h1', 'Ala ma kota'), this.isOpen && h('h3', 'kot robi w ' + this.message),
  //   ])
  // }
}).$mount('#app');


// function renderSidebar(sidebarSelector){
//  return new Vue({...}).mount(sidebarSelector)
// }

// (window as any).app.placki = {}