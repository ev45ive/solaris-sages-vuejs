import { Playlist } from "../../model/Playlist";

export const mockPlaylists: Playlist[] = [
{
id: "123",
name: "Best playlist 123",
public: true,
description: "My fav playlist 123",
},
{
id: "234",
name: "Best playlist 234",
public: false,
description: "My fav 234",
},
{
id: "435",
name: "Best playlist 345",
public: true,
description: "My fav 345",
},
];
