import { Album, assertAlbumSearchResponse, PagingObject, Track } from "@/model/Album"
import { Playlist } from "@/model/Playlist"
import { assertUserProfile, UserProfile } from "@/model/User"
import axios from "axios"


export const searchAlbums = async (query: string): Promise<Album[]> => {
  const res = await axios.get<unknown>(`search`, {
    params: { type: 'album', query }
  })
  assertAlbumSearchResponse(res.data)
  return res.data.albums.items
}

export const getCurrentUser = async (): Promise<UserProfile> => {
  const res = await axios.get<unknown>(`me`)
  assertUserProfile(res.data)
  return res.data
}

export const getCurrentUserPlaylists = async (): Promise<Playlist[]> => {
  const res = await axios.get<PagingObject<Playlist>>(`me/playlists`)
  return res.data.items
}

export const getPlaylistsById = async (id): Promise<Playlist> => {
  const res = await axios.get<Playlist>(`playlists/${id}`)
  return res.data
}
