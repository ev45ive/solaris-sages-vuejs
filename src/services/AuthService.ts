import axios from "axios";

let token = ''

// holoyis165@bulkbye.com
// placki777

axios.defaults.baseURL = 'https://api.spotify.com/v1/'

axios.interceptors.request.use(config => {
  config.headers = {
    ...config.headers,
    'Authorization': 'Bearer ' + getToken()
  };
  return config
})

axios.interceptors.response.use(ok => ok, error => {
  console.error(error)

  if (!axios.isAxiosError(error)) throw new Error('Unexpected error')

  if (!error.response || !isSpotifyError(error.response?.data)) throw new Error('Unexpected error')

  if (error.response.status === 401) { login() }

  throw new Error((error.response.data.error.message))
})

function isSpotifyError(err: any): err is SpotifyError {
  return 'error' in err && 'message' in err.error
}
interface SpotifyError {
  error: {
    message: string, status: number
  }
}


export const init = () => {
  token = new URLSearchParams(window.location.hash).get('#access_token') || '';

  if (token) {
    sessionStorage.setItem('token', token)
  }
  else {
    token = sessionStorage.getItem('token') || ''
  }

  if (!token) {
    login()
  }
}


export const logout = () => {
  token = ''
  sessionStorage.removeItem('token')
}

export const login = () => {
  var client_id = 'a84a38836b5c48078d0c1e3aebe412a3';
  var redirect_uri = window.location.origin + '/';

  var scope = 'user-read-private user-read-email';

  var url = 'https://accounts.spotify.com/authorize';
  url += '?response_type=token';
  url += '&client_id=' + encodeURIComponent(client_id);
  url += '&scope=' + encodeURIComponent(scope);
  url += '&redirect_uri=' + encodeURIComponent(redirect_uri);
  // url += '&state=' + encodeURIComponent(state);

  window.location.href = url;
}

export const getToken = () => {
  return token
}