import PlaylistsView from '@/views/playlists/PlaylistsView.vue';
import SearchViewVue from '@/views/search/SearchView.vue';
import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';
import Home from '../views/Home.vue';
import Counter from '../views/Counter.vue';
import PlaylistsVuexVue from '../views/playlists/PlaylistsVuexVue.vue';
import PlaylistDetailsRouterView from '../views/playlists/PlaylistDetailsRouterView.vue';

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: '/search',
    name: 'Search',
    component: SearchViewVue,
  },
  {
    path: '/playlists',
    name: 'Playlists',
    component: PlaylistsVuexVue,
    children: [
      {
        path: ':playlistId',
        component: PlaylistDetailsRouterView
      },
      {
        path: ':playlistId/edit',
        component: ({
          render(h) { return h('div', 'Hello world') }
        })
      }
    ]
  },
  {
    path: '/counter', component: Counter
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue'),
  },
  {
    path: '/',
    redirect: '/search'
  },
  {
    path: '/access_token*',
    redirect: '/'
  },
];



// playlists/
// playlists/123/show/
// playlists/123/show/tracks

const router = new VueRouter({
  routes,
  linkActiveClass: 'active placki',
  linkExactActiveClass: 'active exact',
  mode: 'history',
});
// router.beforeEach((to,from,next))
// router.afterEach()

export default router;

