import Vue from 'vue';
import UserProfileVue2 from "./UserProfile.vue";
import Card from "./Card.vue";

Vue.component("UserProfile", UserProfileVue2);
Vue.component("Card", Card);
