import { Playlist } from '@/model/Playlist';
import { getCurrentUserPlaylists, getPlaylistsById } from '@/services/APIService';
import { mockPlaylists } from '@/views/playlists/mockPlaylists';
import Vue from 'vue';
import { Module } from 'vuex';
import { RootState } from '.';

interface PlaylistsState {
  // EntitesModule
  entities: {
    playlists: {
      [key: Playlist['id']]: Playlist
    }
  },
  lists: {
    // PlaylistsModule
    playlists: Playlist['id'][],
    drafts: Playlist['id'][],
    // HomeModule
    recent: Playlist['id'][],
    top: Playlist['id'][],
  },
  // PlaylistsModule
  selectedId: Playlist['id'] | null
  // UiModule
  // message: string
  // error: string
  // loading: string
}

// https://github.com/paularmstrong/normalizr

export const playlistsModule: Module<PlaylistsState, RootState> = {
  namespaced: true,
  state: {
    // ...sharedState()
    // data
    entities: { playlists: { /* {id:123} */ } },
    // order
    lists: { playlists: [/* 123,324,456 */], drafts: [], recent: [], top: [] },
    // state
    selectedId: null
  },
  mutations: {
    // ...sharedmutations()
    playlistsLoadSuccess(state, payload: Playlist[]) {
      state.lists.playlists = payload.map(p => p.id)
      payload.forEach(p => Vue.set(state.entities.playlists, p.id, p))
    },
    select(state, id) { state.selectedId = id },
    updatePlaylist(state, playlist) {
      Vue.set(state.entities.playlists, playlist.id, playlist)
    }
  },
  actions: {
    // fakeaction({}, payload) { },
    selectPlaylist({ commit }, id) {
      commit('select', id)
    },
    // fetch vs (stale while revalidate)
    async loadPlaylist({ commit, state, getters }, id) {
      const playlist = await getPlaylistsById(id)
      commit('updatePlaylist', playlist)
      commit('select', id)
    },
    async loadPlaylists({ commit }) {
      commit('ui/loading', true, { root: true })
      const playlists = await getCurrentUserPlaylists()
      commit('playlistsLoadSuccess', playlists)
      commit('ui/loading', true, { root: true })
    }
  },
  getters: {
    playlists: state => state.lists.playlists.map(id => state.entities.playlists[id]),
    selected: (state, getters) => {
      return state.entities.playlists[state.selectedId]
    }
  },
};
