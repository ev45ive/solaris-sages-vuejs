import Vue from 'vue';
import Vuex from 'vuex';
import { playlistsModule } from './playlistsModule';
import { uiModule } from './uiModule';

Vue.use(Vuex);
export interface RootState {
  counter: { value: number }
}

export default new Vuex.Store<RootState>({
  state: {
    counter: {
      value: 0
    }
    // playlists: playlistsModule.state...
  },
  mutations: { // sync
    inc(state, payload = 1) { state.counter.value += payload },
    dec(state, payload = 1) {
      state.counter.value -= payload
      state
    },
  },
  actions: { // async
  },
  modules: {
    playlists: playlistsModule,
    ui: uiModule
  },
  getters: {
    counter(state) {
      return state.counter.value
    },

    loading(state: any) {
      return state.ui.loading
    }
  }
});



// interface UiState { }
// export const uiModule: Module<UiState, RootState> = {
//   namespaced: true,
//   state: {},
//   mutations: {},
//   actions: {},
//   getters: {}
// }