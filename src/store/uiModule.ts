import { Module } from 'vuex';
import { RootState } from './index';

interface UiState {
  message: string,
  loading: boolean,
  error: string
  handle: any
  errorHandle: any
}

export const uiModule: Module<UiState, RootState> = {
  namespaced: true,
  state: {
    error: '',
    loading: false,
    message: '',
    handle: null,
    errorHandle: null
  },
  mutations: {
    message(state, message) { state.message = message },
    loading(state, loading) { state.loading = loading },
    error(state, error) { state.error = error?.message || error },
  },
  actions: {
    message({ commit, state }, message) {
      clearTimeout(state.handle)
      commit('message', message)
      state.handle = setTimeout(() => {
        commit('message', '')
      })
      // or inverval clear old messages from list...
    },
    error({ commit, state }, error) {
      clearTimeout(state.errorHandle)
      commit('error', error)
      state.errorHandle = setTimeout(() => {
        commit('error', '')
      })
    }
  },
  getters: {
  }
};
