import { AlbumItemView } from "./Album";

// faker.js
// casual.js 


export const albumsMocks: AlbumItemView[] = [
  {
    id: '123', name: 'Album 123',
    images: [{ height: 300, width: 300, url: 'https://www.placecage.com/c/200/300' }], type: 'album'
  },
  {
    id: '234', name: 'Album 234',
    images: [{ height: 300, width: 300, url: 'https://www.placecage.com/c/200/300' }], type: 'album'
  },
  {
    id: '345', name: 'Album 345',
    images: [{ height: 300, width: 300, url: 'https://www.placecage.com/c/200/300' }], type: 'album'
  },
  {
    id: '456', name: 'Album 456',
    images: [{ height: 300, width: 300, url: 'https://www.placecage.com/c/200/300' }], type: 'album'
  },
]