//@ts-check

// npm i -g typescript
// tsc --init --allowJs --checkJs false

/**
 * @typedef Album2
 * @property {string} name
 */

/**
 * @typedef Album 
 * @type {import('./Album').Album}
 */

/** @type Album[] */
const albums = []

albums.push({
  // @ts-ignore
  x: 123
})